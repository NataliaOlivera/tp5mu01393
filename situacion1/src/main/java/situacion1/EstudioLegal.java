package situacion1;

import situacion1.Excepciones.ClienteException;
import java.util.ArrayList;
import java.util.Collections;
import situacion1.Persistencia.ICliente;

public class EstudioLegal {
    private final ICliente clientes;
    
    public EstudioLegal(ICliente cuenta) {
        this.clientes=cuenta;
    }

    public void agregarCliente(Cliente cliente) {
        clientes.insert(cliente);
    }
    
    public ArrayList<Cliente> getClientes(){
        return (ArrayList) clientes.findAll();
    }
    
    public Cliente buscarCliente(Integer dni){
        Cliente clienteEncontrado=clientes.findByPK(dni);
        if (clienteEncontrado==null){
            throw new ClienteException("El cliente con dni : "+dni+" no exite");
        }
        return clienteEncontrado;
    }
    
    public void eliminarCliente(Cliente cliente) {
        clientes.delete(cliente);
    }

    
    public ArrayList<Cliente> listarClientesPorNumeroDeDni(){       
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) this.clientes.findAll();  
        Collections.sort(clientes);  
        return clientes;          
    }
    
    public ArrayList<Cliente> listarClientesPorApellido(){       
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) this.clientes.findAll();  
        Collections.sort(clientes, new CompararCliente());  
        return clientes;          
    }

 

}
package situacion1;

public class DomicilioBarrial extends Domicilio {
    private String barrio;
    private String manzana;
    private Integer numero;

    public DomicilioBarrial(String calle, String barrio, String manzana, Integer numero) {
        super(calle);
        this.barrio = barrio;
        this.manzana = manzana;
        this.numero = numero;
    }


    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String listarDatosDelDomicilio(){
       return ("B� "+getBarrio()+" ,"+super.listarDatosDelDomicilio()+" , Mnz "+getManzana()+" , N� "+getNumero());
    }
}

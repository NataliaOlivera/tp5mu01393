package situacion1.Excepciones;

public class DomicilioNuloException extends RuntimeException{

    public DomicilioNuloException() {
        super("No se selecciono tipo de domicilio");
    }
    
}

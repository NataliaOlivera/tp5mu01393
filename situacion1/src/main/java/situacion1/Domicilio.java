package situacion1;

public abstract class Domicilio {
    private String calle;

    public Domicilio(String calle) {
        this.calle = calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String listarDatosDelDomicilio(){
        return (getCalle());
    }

}

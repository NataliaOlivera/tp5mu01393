package situacion1;

import java.util.Comparator;

public class CompararCliente implements Comparator<Cliente>{
    @Override
	public int compare(Cliente c1, Cliente c2) {
		// TODO Auto-generated method stub
		return c1.getApellido().compareToIgnoreCase(c2.getApellido());
	}
}

package situacion1;

public class DomicilioEdificio extends Domicilio{
    private Integer piso;
    private Integer departamento;

    public DomicilioEdificio(String calle, Integer piso, Integer departamento) {
        super(calle);
        this.piso = piso;
        this.departamento = departamento;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public Integer getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Integer departamento) {
        this.departamento = departamento;
    }

    public String listarDatosDelDomicilio(){
        return(super.listarDatosDelDomicilio()+" ,Piso "+getPiso()+" ,Dpt "+getDepartamento());
    }
    
}

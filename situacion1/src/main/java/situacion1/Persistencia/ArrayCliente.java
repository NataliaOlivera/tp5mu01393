/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion1.Persistencia;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import situacion1.Cliente;
import situacion1.Excepciones.ClienteException;

/**
 *
 * @author nati2_000
 */
public class ArrayCliente implements ICliente{
     private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private Component rootPane;
    
     @Override
    public void delete(Cliente cliente) {
         clientes.remove(cliente);               
    }
    @Override
    public Cliente findByPK(Integer dni)  {
        Cliente encontrado = null;
         for (Cliente aux : clientes) {
            if (aux.getDni().equals(dni)){
                encontrado=aux;
            }
        }
        return encontrado;
    }
    
    

    @Override
    public Collection findAll() {
        return clientes;
    }
    
    public ArrayList<Cliente> retornarClientes() {
        return clientes;
    }

    @Override
    public void insert(Cliente insertRecord) {
        Cliente existente = findByPK(insertRecord.getDni());
        if (existente != null) {        
           throw new ClienteException("Cliente con DNI:"+insertRecord.getDni()+" ya existe");         
        }
        clientes.add(insertRecord);
         
    }

    @Override
    public void update(Cliente updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

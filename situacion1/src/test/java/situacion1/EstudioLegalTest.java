package situacion1;

import org.junit.Test;

import situacion1.Excepciones.ClienteException;
import situacion1.Persistencia.ArrayCliente;

import static org.junit.Assert.*;
import situacion1.Persistencia.ICliente;

public class EstudioLegalTest {

    @Test
    public void agregarClientes(){
        ICliente cuenta= new ArrayCliente();
        EstudioLegal estudioLegal= new EstudioLegal(cuenta);
        Domicilio domicilio1= new DomicilioEdificio("Calle", 23, 15);
        Cliente cliente1=new Cliente("Olivera", "Natalia", 40598438,"Consulta","nati@gmail.com",13344,domicilio1);
        Domicilio domicilio2= new DomicilioBarrial("Av Ocampo", "Barrio","Manzana 33" ,56 );
        Cliente cliente2=new Cliente("Pereyra", "Evelyn", 322442,"Consulta legal","eve@gmail.com",144,domicilio2);

        estudioLegal.agregarCliente(cliente1);
        estudioLegal.agregarCliente(cliente2);

        assertEquals(2,estudioLegal.getClientes().size());
    }


    @Test (expected = ClienteException.class)
    public void verificarQueNoSeAgregueClienteMasDeUnaVez(){
        ICliente cuenta= new ArrayCliente();
        EstudioLegal estudioLegal= new EstudioLegal(cuenta);
        Domicilio domicilio1= new DomicilioEdificio("Calle", 23, 15);
        Cliente cliente1=new Cliente("Olivera", "Natalia", 40598438,"Consulta","nati@gmail.com",13344,domicilio1);
        Cliente cliente2=new Cliente("Olivera", "Natalia", 40598438,"Consulta","nati@gmail.com",13344,domicilio1);
        estudioLegal.agregarCliente(cliente1);
        estudioLegal.agregarCliente(cliente2);
    }
}

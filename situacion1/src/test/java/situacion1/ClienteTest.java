package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteTest {
    
    @Test 
    public void verSiAgregaBienDomicilio(){    
        Domicilio domicilio= new DomicilioEdificio("Calle", 23, 15);   
        Cliente cliente=new Cliente("Olivera", "Natalia", 40598438,"Consulta","nati@gmail.com",13344,domicilio);
        String domicilioEsperado =  cliente.getDomicilio().getCalle();
        assertEquals("Calle", domicilioEsperado);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.Excepciones;

/**
 *
 * @author nati2_000
 */
public class MateriaPrimaExistenteException extends RuntimeException{
    
    public MateriaPrimaExistenteException(String mensaje){
        super(mensaje);
    }
    
}

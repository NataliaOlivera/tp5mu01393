package situacion2;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import situacion2.Excepciones.MateriaPrimaInexistenteException;
import situacion2.Excepciones.ProductoInexistenteException;
import situacion2.Persistencia.IMateriaPrima;
import situacion2.Persistencia.IVenta;

public class Empresa {
    private final IVenta venta;
    private final IMateriaPrima materiasPrimas;

    public Empresa(IVenta venta, IMateriaPrima materiaPrima) {
        this.venta=venta;
        this.materiasPrimas=materiaPrima;
    }

    public ArrayList<OrdenDeVenta> getOrdenesVenta() {
        return (ArrayList) venta.findAll();
    }

    public void agregarOrdenesDeVenta(OrdenDeVenta ordenDeVenta) {
        venta.insert(ordenDeVenta);
    }
    
     public ArrayList<MateriaPrima> getMateriasPrimas() {
        return (ArrayList) materiasPrimas.findAll();
    }

    public void agregarMateriasPrimas(MateriaPrima materiaPrima) {
        materiasPrimas.insert(materiaPrima);
    }

    public ArrayList<OrdenDeVenta> listarVentaPorNombreDelProducto(){       
        ArrayList<OrdenDeVenta> productos = (ArrayList<OrdenDeVenta>) this.venta.findAll();  
        Collections.sort(productos, new CompararPorNombre());  
        return productos;          
    }
    
    public ArrayList<OrdenDeVenta> listarVentaPorPrecioDelProducto(){       
        ArrayList<OrdenDeVenta> productos = (ArrayList<OrdenDeVenta>) this.venta.findAll();  
        Collections.sort(productos, new CompararPorPrecio());  
        return productos;          
    }

    public ArrayList<MateriaPrima> listarMateriasPrimasPorCodigo(){       
        ArrayList<MateriaPrima> materias = (ArrayList<MateriaPrima>) this.materiasPrimas.findAll();  
        Collections.sort(materias);  
        return materias;          
    }
    
    public BigDecimal totalGanancia(){
        BigDecimal total= new BigDecimal(0);
        for(OrdenDeVenta var: getOrdenesVenta()){
            total=total.add(var.getProducto().getPrecio());      
        }
        return total;
    }
    
    public BigDecimal totalGastos(){
        BigDecimal total= new BigDecimal(0);
        for(MateriaPrima var: getMateriasPrimas()){
            total=total.add(var.getPrecio());      
        }
        return total;
    }
    
    public OrdenDeVenta buscarProducto(Integer codigo){
        OrdenDeVenta productoEncontrado=venta.findByPK(codigo);
        if (productoEncontrado==null){
            throw new ProductoInexistenteException("El producto con codigo : "+codigo+" no exite");
        }
        return productoEncontrado;
    }
    
    public void eliminarProducto(OrdenDeVenta producto) {
        venta.delete(producto);
    }
    
    public MateriaPrima buscarMateriaPrima(Integer codigo){
        MateriaPrima materiaEncontrado=materiasPrimas.findByPK(codigo);
        if (materiaEncontrado==null){
            throw new MateriaPrimaInexistenteException("La Materia Prima con codigo : "+codigo+" no exite");
        }
        return materiaEncontrado;
    }
    
    public void eliminarMateriaPrima(MateriaPrima materiaPrima) {
        materiasPrimas.delete(materiaPrima);
    }
}

package situacion2;

import java.math.BigDecimal;

public class HerramientaElectrica extends Herramienta implements Producto{
    private BigDecimal consumo;

    public HerramientaElectrica(Integer codigo, String nombre, BigDecimal precio, String funcionalidad,
            BigDecimal consumo) {
        super(codigo, nombre, precio, funcionalidad);
        this.consumo = consumo;
    }

    public BigDecimal getConsumo() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo = consumo;
    }
}

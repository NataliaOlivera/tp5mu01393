package situacion2;

public class OrdenDeVenta {
    private Producto producto;

    public OrdenDeVenta(Producto producto) {
        this.producto=producto;
    }
    
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.Persistencia;

import java.util.Collection;
import situacion2.MateriaPrima;

/**
 *
 * @author nati2_000
 */
public interface IMateriaPrima {
    public void delete(MateriaPrima materiaPrima);
    public MateriaPrima findByPK(Integer codigo);
    public Collection findAll();
    public void insert(MateriaPrima insertRecord);
    public void update(MateriaPrima updateRecord);    
}

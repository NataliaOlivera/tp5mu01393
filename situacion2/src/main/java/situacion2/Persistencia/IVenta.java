/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.Persistencia;

import java.util.Collection;
import situacion2.OrdenDeVenta;

/**
 *
 * @author nati2_000
 */
public interface IVenta {
    public void delete(OrdenDeVenta producto);
    public OrdenDeVenta findByPK(Integer codigoDeVenta);
    public Collection findAll();
    public void insert(OrdenDeVenta insertRecord);
    public void update(OrdenDeVenta updateRecord);
    
}

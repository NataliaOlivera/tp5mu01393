/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.Persistencia;

import java.util.ArrayList;
import java.util.Collection;
import situacion2.Excepciones.ProductoExistenteException;
import situacion2.OrdenDeVenta;

/**
 *
 * @author nati2_000
 */
public class ArrayVenta implements IVenta{
    private ArrayList<OrdenDeVenta> ordenes= new ArrayList<OrdenDeVenta>();
    
    @Override
    public void delete(OrdenDeVenta producto) {
            ordenes.remove(producto);            

        }        
        //throw new ClienteNoEncontradoExceptio("Dni inexistente: " + dni);

    @Override
    public OrdenDeVenta findByPK(Integer codigoDeVenta) {
        OrdenDeVenta encontrado = null;
        for (OrdenDeVenta aux : ordenes) {
            if (aux.getProducto().getCodigo().equals(codigoDeVenta)){
                encontrado=aux;
            }
        }
        return encontrado;
    }

    @Override
    public Collection findAll() {
        return ordenes;
    }

    @Override
    public void insert(OrdenDeVenta insertRecord) {
        OrdenDeVenta existente = findByPK(insertRecord.getProducto().getCodigo());
        if (existente != null) {        
           throw new ProductoExistenteException("Producto con codigo :"+insertRecord.getProducto().getCodigo()+" ya existe");         
        }
        ordenes.add(insertRecord); 
    }

    @Override
    public void update(OrdenDeVenta updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.Persistencia;

import situacion2.Excepciones.MateriaPrimaExistenteException;
import java.util.ArrayList;
import java.util.Collection;
import situacion2.MateriaPrima;

/**
 *
 * @author nati2_000
 */
public class ArrayMateriaPrima implements IMateriaPrima{
    private ArrayList <MateriaPrima> primas =new ArrayList<MateriaPrima>();
    
    @Override
    public void delete(MateriaPrima materiaPrima) {
          primas.remove(materiaPrima);            
    }        
        //throw new ClienteNoEncontradoExceptio("Dni inexistente: " + dni);

    @Override
    public MateriaPrima findByPK(Integer codigo) {
       MateriaPrima encontrado = null;
        for (MateriaPrima aux : primas) {
            if (aux.getCodigo().equals(codigo)){
                encontrado=aux;
            }
        }
        return encontrado;
    }

    @Override
    public Collection findAll() {
        return primas;
    }

    @Override
    public void insert(MateriaPrima insertRecord) {
        MateriaPrima existente = findByPK(insertRecord.getCodigo());
        if (existente != null) {                           
            throw new MateriaPrimaExistenteException("Materia Prima con codigo :"+insertRecord.getCodigo()+" ya existe");
        }
        primas.add(insertRecord); 
    }

    @Override
    public void update(MateriaPrima updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

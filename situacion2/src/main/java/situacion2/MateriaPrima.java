package situacion2;

import java.math.BigDecimal;

public class MateriaPrima extends Item implements Comparable<MateriaPrima>{
    
    private String proveedor;

    public MateriaPrima(Integer codigo, String nombre, BigDecimal precio, String nombreDelProveedor) {
        super(codigo, nombre, precio);
        this.proveedor = nombreDelProveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }
    
    @Override
    public int compareTo(MateriaPrima c){
        if(c.getCodigo()>getCodigo()){
            return -1;
        }
        else{
            if(c.getCodigo()>getCodigo()){
                return 0;
            }
            else{
                return 1;
            }
        }
        
    }
}

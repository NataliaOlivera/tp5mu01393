/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2;

import java.util.Comparator;

/**
 *
 * @author nati2_000
 */
public class CompararPorNombre implements Comparator<OrdenDeVenta>{
    @Override
	public int compare(OrdenDeVenta o1, OrdenDeVenta o2) {
		// TODO Auto-generated method stub
		return o1.getProducto().getNombre().compareToIgnoreCase(o2.getProducto().getNombre());
	}
    
}

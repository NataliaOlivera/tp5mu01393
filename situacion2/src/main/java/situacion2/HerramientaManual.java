package situacion2;

import java.math.BigDecimal;

public class HerramientaManual extends Herramienta implements Producto{

	public HerramientaManual(Integer codigo, String nombre, BigDecimal precio, String funcionalidad) {
		super(codigo, nombre, precio, funcionalidad);
	} 

    @Override
    public BigDecimal getConsumo() {
         return new BigDecimal(0);
    }
        
}

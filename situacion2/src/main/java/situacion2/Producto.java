package situacion2;

import java.math.*;

public interface Producto {
    public Integer getCodigo();
    public String getNombre();
    public BigDecimal getPrecio();  
    public String getFuncionalidad();
    public BigDecimal getConsumo();
}

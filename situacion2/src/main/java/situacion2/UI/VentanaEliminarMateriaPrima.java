/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.UI;

import javax.swing.JOptionPane;
import situacion2.Empresa;
import situacion2.Excepciones.CamposIncompletosException;
import situacion2.Excepciones.MateriaPrimaInexistenteException;
import situacion2.MateriaPrima;

/**
 *
 * @author nati2_000
 */
public class VentanaEliminarMateriaPrima extends javax.swing.JDialog {
    private Empresa empresa1;
    /**
     * Creates new form VentanaEliminarMateriaPrima
     */
    public VentanaEliminarMateriaPrima(java.awt.Frame parent, boolean modal, Empresa empresa1) {
        super(parent, modal);
        this.empresa1=empresa1;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textoPrincipalJLabel = new javax.swing.JLabel();
        codigoJTextField = new javax.swing.JTextField();
        eliminarJButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        textoPrincipalJLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textoPrincipalJLabel.setText("Ingrese el codigo de la Materia Prima a eliminar");

        eliminarJButton.setText("Eliminar");
        eliminarJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(textoPrincipalJLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(codigoJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(eliminarJButton)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(textoPrincipalJLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(codigoJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(eliminarJButton)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void eliminarJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarJButtonActionPerformed
        // TODO add your handling code here:
        try{
            if((codigoJTextField.getText() != null && codigoJTextField.getText().equals(""))){
                throw new CamposIncompletosException("Casillero vacio");
            }
            MateriaPrima materiaEncontrado= empresa1.buscarMateriaPrima(Integer.parseInt(this.codigoJTextField.getText()));
            int opcion= JOptionPane.showConfirmDialog(this,"�Seguro desea eliminar el Producto  *"+materiaEncontrado.getNombre()+"* ?");
            if(opcion==JOptionPane.YES_OPTION){
                empresa1.eliminarMateriaPrima(materiaEncontrado);
                JOptionPane.showMessageDialog(rootPane, "Materia Prima eliminada ", "Alerta",1);
                getParent().setVisible(true);
                dispose();
            }else{
                if(opcion== JOptionPane.NO_OPTION){
                    getParent().setVisible(true);
                    dispose();
                }
            }
        }catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane,"Caracter no valido", "Alerta",0);
            codigoJTextField.setText("");
        } catch (CamposIncompletosException c){
            JOptionPane.showMessageDialog(rootPane, c.getMessage(), "Alerta",0);           
        }catch(MateriaPrimaInexistenteException c){
            JOptionPane.showMessageDialog(rootPane, c.getMessage(), "Alerta",0);
            codigoJTextField.setText("");
        }
    }//GEN-LAST:event_eliminarJButtonActionPerformed

    /**
     * @param args the command line arguments
     */
   

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField codigoJTextField;
    private javax.swing.JButton eliminarJButton;
    private javax.swing.JLabel textoPrincipalJLabel;
    // End of variables declaration//GEN-END:variables

}

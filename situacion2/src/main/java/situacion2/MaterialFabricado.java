package situacion2;

import java.math.BigDecimal;

public class MaterialFabricado extends Item implements Producto{

    public MaterialFabricado(Integer codigo, String nombre, BigDecimal precio) {
        super(codigo, nombre, precio);
    } 

    @Override
    public String getFuncionalidad() {
        return ("");
    }

    @Override
    public BigDecimal getConsumo() {
        return new BigDecimal(0);
    }
}

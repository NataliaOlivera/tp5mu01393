package situacion2;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;
import situacion2.Excepciones.MateriaPrimaInexistenteException;
import situacion2.Excepciones.ProductoInexistenteException;
import situacion2.Persistencia.ArrayMateriaPrima;
import situacion2.Persistencia.ArrayVenta;
import situacion2.Persistencia.IMateriaPrima;
import situacion2.Persistencia.IVenta;

public class EmpresaTest {

    @Test
    public void agregarOrdenesDeVenta(){
        IVenta venta= new ArrayVenta();
        IMateriaPrima materiasPrimas= new ArrayMateriaPrima();
        Empresa empresa= new Empresa(venta,materiasPrimas);
        MaterialFabricado material1= new MaterialFabricado(222, "Material", new BigDecimal(233));
        OrdenDeVenta venta1= new OrdenDeVenta(material1);

        empresa.agregarOrdenesDeVenta(venta1);

        assertEquals(1,empresa.getOrdenesVenta().size());
    }
    
    @Test
    public void calcularTotalDeGanancia(){ 
        IVenta venta= new ArrayVenta();
        IMateriaPrima materiaPrima = new ArrayMateriaPrima();
        Empresa empresa= new Empresa(venta, materiaPrima);
        MaterialFabricado materialFabricado= new MaterialFabricado(3444, "Ladrillo", new BigDecimal(30));
        OrdenDeVenta ordenDeVenta1= new OrdenDeVenta(materialFabricado);
        HerramientaManual herramientaManual= new HerramientaManual(234, "Martillo", new BigDecimal (100), "Martillar");
        OrdenDeVenta ordenDeVenta2= new OrdenDeVenta(herramientaManual);
        HerramientaElectrica herramientaElectrica= new HerramientaElectrica(675, "Taladro", new BigDecimal(500), "Taladrar", new BigDecimal(45));
        OrdenDeVenta ordenDeVenta3= new OrdenDeVenta(herramientaElectrica);
        
        BigDecimal totalEsperado= empresa.totalGanancia();

        assertEquals(new BigDecimal(630), totalEsperado);
    }
    
    @Test
    public void calcularTotalGastos(){
        IVenta venta= new ArrayVenta();
        IMateriaPrima materiaPrima = new ArrayMateriaPrima();
        Empresa empresa= new Empresa(venta, materiaPrima);
        MateriaPrima materia1= new MateriaPrima(123, "Cal", new BigDecimal(100), "Proveedor");
        MateriaPrima materia2= new MateriaPrima(313, "Cemento", new BigDecimal(200), "Proveedor");
        empresa.agregarMateriasPrimas(materia1);
        empresa.agregarMateriasPrimas(materia2);
        
        BigDecimal totalEsperado= empresa.totalGastos();
        
        assertEquals(new BigDecimal(300), totalEsperado);
    }
    
    @Test (expected = ProductoInexistenteException.class)
    public void buscarProductoQueNoExiste(){
        IVenta venta= new ArrayVenta();
        IMateriaPrima materiaPrima = new ArrayMateriaPrima();
        Empresa empresa= new Empresa(venta, materiaPrima);
        empresa.buscarProducto(555);
    } 
    
    
    @Test (expected = MateriaPrimaInexistenteException.class)
    public void buscarMateriaPrimaQueNoExiste(){
        IVenta venta= new ArrayVenta();
        IMateriaPrima materiaPrima = new ArrayMateriaPrima();
        Empresa empresa= new Empresa(venta, materiaPrima);
        empresa.buscarMateriaPrima(555);
    }

    
}
